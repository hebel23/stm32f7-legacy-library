#include "stm32f7xx.h"

static uint32_t SystemTickCount = 0;

static void SystemClock_Config(void);
static void SysTick_Configuration(void);
static void MPU_Config(void);
static void CPU_CACHE_Enable(void);
uint32_t GetSysTickTickCount(void);

int main(void)
{
  GPIO_InitTypeDef gpio_initstruct;
  uint32_t Led_1_Tick, Led_2_Tick;
  uint8_t LedState;
  
  MPU_Config();
  CPU_CACHE_Enable();
  FLASH_AdaptiveRealTimeCmd(ENABLE);
  SystemClock_Config();
  SysTick_Configuration();
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
  
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  
  gpio_initstruct.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_14;
  gpio_initstruct.GPIO_Mode = GPIO_Mode_OUT;
  gpio_initstruct.GPIO_OType = GPIO_OType_PP;
  gpio_initstruct.GPIO_PuPd = GPIO_PuPd_UP;
  gpio_initstruct.GPIO_Speed = GPIO_Low_Speed;
  
  GPIO_Init(GPIOB, &gpio_initstruct);
  
  GPIO_SetBits(GPIOB, GPIO_Pin_7 | GPIO_Pin_14);
  GPIO_ResetBits(GPIOB, GPIO_Pin_7 | GPIO_Pin_14);
  
  Led_1_Tick = 0;
  Led_2_Tick = 0;
  LedState = 0;
  
  while(1)
  {
    if(GetSysTickTickCount() - Led_1_Tick > 100)
    {
      if(!(LedState & 0x01))
      {
        GPIO_SetBits(GPIOB, GPIO_Pin_7);
        LedState |= 0x01;
      }
      else
      {
        GPIO_ResetBits(GPIOB, GPIO_Pin_7);
        LedState &= ~0x01;
      }
      
      Led_1_Tick = GetSysTickTickCount();
    }
    
    if(GetSysTickTickCount() - Led_2_Tick > 600)
    {
      if(!(LedState & 0x02))
      {
        GPIO_SetBits(GPIOB, GPIO_Pin_14);
        LedState |= 0x02;
      }
      else
      {
        GPIO_ResetBits(GPIOB, GPIO_Pin_14);
        LedState &= ~0x02;
      }
      
      Led_2_Tick = GetSysTickTickCount();
    }
  }
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 216000000
  *            HCLK(Hz)                       = 216000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 8
  *            PLL_N                          = 432
  *            PLL_P                          = 2
  *            PLL_Q                          = 9
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_HSEConfig(RCC_HSE_ON);
  
  if ( RCC_WaitForHSEStartUp() != SUCCESS )
  {
    while(1);
  }
  
  RCC_PLLConfig(RCC_PLLSource_HSE, 8, 432, 2, 9);
  RCC_PLLCmd(ENABLE);
  
  while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);
  
  /* Activate the OverDrive to reach the 216 MHz Frequency */  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
  
  PWR_OverDriveCmd( ENABLE );
  
  while( PWR_GetFlagStatus( PWR_FLAG_ODRDY ) == RESET );
  
  while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET);
  
  PWR_OverDriveSWCmd(ENABLE);
  
  while( PWR_GetFlagStatus( PWR_FLAG_ODSWRDY ) == RESET );
  
  FLASH_SetLatency(FLASH_Latency_7);
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
  
  RCC_HCLKConfig(RCC_SYSCLK_Div1);
  RCC_PCLK1Config(RCC_HCLK_Div4);
  RCC_PCLK2Config(RCC_HCLK_Div2);
  
  SystemCoreClockUpdate();
}

/**
  * @brief  SysTick_Configuration
  * @param  None
  * @retval None
  */
static void SysTick_Configuration(void)
{
  RCC_ClocksTypeDef RCC_Clocks;
  
  /* Configure Systick clock source as HCLK */
  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);

  /* SystTick configuration: an interrupt every 1ms */
  RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config( ( RCC_Clocks.HCLK_Frequency / ( 1000 ) ) );
}

/**
  * @brief  Configure the MPU attributes as Write Through for SRAM1/2.
  * @note   The Base Address is 0x20010000 since this memory interface is the AXI.
  *         The Region Size is 256KB, it is related to SRAM1 and SRAM2  memory size.
  * @param  None
  * @retval None
  */
static void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct;
  
  /* Disable the MPU */
  MPU_Disable();

  /* Configure the MPU attributes as WT for SRAM */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x20020000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  MPU_ConfigRegion(&MPU_InitStruct);

  /* Enable the MPU */
  MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
  /* Enable branch prediction */
  SCB->CCR |= (1 <<18); 
  __DSB();

  /* Enable I-Cache */
  SCB_EnableICache();	
	
  /* Enable D-Cache */
  SCB_EnableDCache();
}

uint32_t GetSysTickTickCount(void)
{
  return SystemTickCount;
}

void SysTickCountIncrease(void)
{
  SystemTickCount++;
}