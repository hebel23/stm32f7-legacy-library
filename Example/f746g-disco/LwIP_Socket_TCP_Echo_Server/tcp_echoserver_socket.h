/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TCP_ECHOSERVER_SOCKET_H__
#define __TCP_ECHOSERVER_SOCKET_H__

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void tcp_echo_server_socket_init(void);

#endif /* __TCP_ECHOSERVER_SOCKET_H__ */
