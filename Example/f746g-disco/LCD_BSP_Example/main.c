#include "stm32f7xx.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_sdram.h"
#include "stm32746g_discovery_lcd.h"

void SystemClock_Config(void);
void SystemCoreClockUpdate(void);
static void LCD_Config( uint32_t LCD_Ram_Address );
static void CPU_CACHE_Enable(void);
static void MPU_Config(void);

int main(void)
{
  MPU_Config();
  CPU_CACHE_Enable();
  FLASH_AdaptiveRealTimeCmd(ENABLE);
  SystemClock_Config();
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
  
  /* Initialize FMC External SDRAM */
  BSP_SDRAM_Init();
  
  /* Initialize LTDC TFT LCD (Use External SDRAM) */
  LCD_Config( SDRAM_DEVICE_ADDR );
  
  BSP_LCD_DisplayStringAtLine( 0, "Hello world!" );
  
  while(1);
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 216000000
  *            HCLK(Hz)                       = 216000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 432
  *            PLL_P                          = 2
  *            PLL_Q                          = 9
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_HSEConfig(RCC_HSE_ON);
  
  if ( RCC_WaitForHSEStartUp() != SUCCESS )
  {
    while(1);
  }
  
  RCC_PLLConfig(RCC_PLLSource_HSE, 25, 432, 2, 9);
  RCC_PLLCmd(ENABLE);
  
  while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);
  
  /* Activate the OverDrive to reach the 216 MHz Frequency */  
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_PWR, ENABLE);
  
  PWR_OverDriveCmd( ENABLE );
  
  while( PWR_GetFlagStatus( PWR_FLAG_ODRDY ) == RESET );
  
  while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET);
  
  PWR_OverDriveSWCmd(ENABLE);
  
  while( PWR_GetFlagStatus( PWR_FLAG_ODSWRDY ) == RESET );
  
  FLASH_SetLatency(FLASH_Latency_7);
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
  
  RCC_HCLKConfig(RCC_SYSCLK_Div1);
  RCC_PCLK1Config(RCC_HCLK_Div4);
  RCC_PCLK2Config(RCC_HCLK_Div2);
  
  SystemCoreClockUpdate();
}

/**
  * @brief  LCD configuration
  * @param  None
  * @retval None
  */
static void LCD_Config( uint32_t LCD_Ram_Address )
{
  /* LCD Initialization */ 
  BSP_LCD_Init();

  /* LCD Initialization */ 
  BSP_LCD_LayerDefaultInit( 0, LCD_Ram_Address);
  //BSP_LCD_LayerDefaultInit( 1, LCD_Ram_Address + (BSP_LCD_GetXSize() * BSP_LCD_GetYSize() * 4 ));
  BSP_LCD_LayerRgb565Init( 1, LCD_Ram_Address + (BSP_LCD_GetXSize() * BSP_LCD_GetYSize() * 4 ));
  
  /* Enable the LCD */ 
  BSP_LCD_DisplayOn(); 
  
  /* Select the LCD Background Layer  */
  BSP_LCD_SelectLayer(LCD_BACKGROUND_LAYER);

  /* Clear the Background Layer */ 
  BSP_LCD_Clear(LCD_COLOR_BLACK);
  
  /* Select the LCD Foreground Layer  */
  BSP_LCD_SelectLayer(LCD_FOREGROUND_LAYER);
  
  /* Clear the Foreground Layer */ 
  BSP_LCD_Clear(LCD_COLOR_BLACK);
  
  /* Configure the transparency for foreground and background :
     Increase the transparency */
  BSP_LCD_SetTransparency(LCD_BACKGROUND_LAYER, 255);
  BSP_LCD_SetTransparency(LCD_FOREGROUND_LAYER, 255);
  
  BSP_LCD_SetTextColor( LCD_COLOR_WHITE );
  BSP_LCD_SetBackColor( LCD_COLOR_TRANSPARENT );
}

/**
  * @brief  Configure the MPU attributes as Write Through for SRAM1/2.
  * @note   The Base Address is 0x20010000 since this memory interface is the AXI.
  *         The Region Size is 256KB, it is related to SRAM1 and SRAM2  memory size.
  * @param  None
  * @retval None
  */
static void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct;
  
  /* Disable the MPU */
  MPU_Disable();

  /* Configure the MPU attributes as WT for SRAM */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x20010000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  MPU_ConfigRegion(&MPU_InitStruct);

  /* Enable the MPU */
  MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
  /* Enable branch prediction */
  SCB->CCR |= (1 <<18); 
  __DSB();

  /* Enable I-Cache */
  SCB_EnableICache();	
	
  /* Enable D-Cache */
  SCB_EnableDCache();
}

void assert_failed(uint8_t* file, uint32_t line)
{
  
}